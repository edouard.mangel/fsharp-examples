﻿namespace ExemplesCSharp {
    public class Examples {

        public TimeSpan Age(DateTime dateNaissance)
        {
            DateTime then = dateNaissance;  
            DateTime now = DateTime.Now;
            return then - now;
        }

        public TimeSpan AgeIn2021(DateTime dateNaissance)
        {
            return new DateTime(2021, 1, 1) - dateNaissance.Date;
        }

        public decimal Diviser(int numerateur, int diviseur)
        {
            return numerateur / diviseur;   
        }

        public int Addition(int numerateur, int diviseur){
            return numerateur / diviseur;
        }


    }
}