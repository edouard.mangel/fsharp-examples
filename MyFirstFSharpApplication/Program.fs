﻿open System

let hello = "Hello from F#"
// Définir une fonction est très simple : 
let HelloFromFSharp() = printfn "%s "hello

// Appeler la fonction aussi. 
HelloFromFSharp()
