﻿module Syntaxic_examples
open System

// single line comments use a double slash
(* multi line comments use (* . . . *) pair

-end of multi line comment- *)

// ======== "Variables" (but not really) ==========
// The "let" keyword defines an (immutable) value
let myInt = 5
let myFloat = 3.14
let myString = "hello" //note that no types needed

// ======== Lists ============
let twoToFive = [2;3;4;5]        // Square brackets create a list with
                                 // semicolon delimiters.

let oneToFive = 1 :: twoToFive   // :: creates list with new 1st element
// The result is [1;2;3;4;5]
let zeroToFive = [0;1] @ twoToFive   // @ concats two lists

// IMPORTANT: commas are never used as delimiters, only semicolons!

// ======== Functions ========
// The "let" keyword also defines a named function.
let square x = x * x                        // Note that no parens are used.
square 3 |> printfn "Square of 3 is %i"     // Now run the function. Again, no parens.

let Add x y = x + y           // don't use Add (x,y)! It means something
                              // completely different.

// add 2 3 |> printfn "Addition of 2 and 3 is %i"                       // Now run the function.

// to define a multiline function, just use indents. No semicolons needed.
let evens list =
   let isEven x = x%2 = 0     // Define "isEven" as an inner ("nested") function
   List.filter isEven list    // List.filter is a library function
                              // with two parameters: a boolean function
                              // and a list to work on

// evens oneToFive |> printfn "OneToFive is %A"              // Now run the function

// You can use parens to clarify precedence. In this example,
// do "map" first, with two args, then do "sum" on the result.
// Without the parens, "List.map" would be passed as an arg to List.sum
let sumOfSquaresTo100 =
   List.sum ( List.map square [1..100] )

// You can pipe the output of one operation to the next using "|>"
// Here is the same sumOfSquares function written using pipes
let sumOfSquaresTo100piped =
   [1..100] |> List.map square |> List.sum  // "square" was defined earlier

// you can define lambdas (anonymous functions) using the "fun" keyword
let sumOfSquaresTo100withFun =
   [1..100] |> List.map (fun x->x*x) |> List.sum

// In F# returns are implicit -- no "return" needed. A function always
// returns the value of the last expression used.

// ======== Pattern Matching ========
// Match..with.. is a supercharged case/switch statement.
let simplePatternMatch =
   let x = "a"
   match x with
    | "a" -> printfn "x is a"
    | "b" -> printfn "x is b"
    | _ -> printfn "x is something else"   // underscore matches anything

// Some(..) and None are roughly analogous to Nullable wrappers
let validValue = Some(99)
let invalidValue = None

// In this example, match..with matches the "Some" and the "None",
// and also unpacks the value in the "Some" at the same time.
let optionPatternMatch input =
   match input with
    | Some i -> printfn "input is an int=%d" i
    | None -> printfn "input is missing"

optionPatternMatch validValue
optionPatternMatch invalidValue

// ========= Complex Data Types =========

// Tuple types are pairs, triples, etc. Tuples use commas.
let twoTuple = 1,2
let threeTuple = "a",2,true

// Record types have named fields. Semicolons are separators.
type Person = {First:string; Last:string}
let person1 = {First="john"; Last="Doe"}

// Union types have choices. Vertical bars are separators.
type Temp =
  | DegreesC of float
  | DegreesF of float
let temp = DegreesF 98.6

// Types can be combined recursively in complex ways.
// E.g. here is a union type that contains a list of the same type:
type Employee =
  | Worker of Person
  | Manager of Employee list
let jdoe = {First="John";Last="Doe"}
let worker = Worker jdoe

// ========= Printing =========
// The printf/printfn functions are similar to the
// Console.Write/WriteLine functions in C#.
printfn "Printing an int %i, a float %f, a bool %b" 1 2.0 true
printfn "A string %s, and something generic %A" "hello" [1;2;3;4]

// all complex types have pretty printing built in
printfn "twoTuple=%A,\nPerson=%A,\nTemp=%A,\nEmployee=%A"
         twoTuple person1 temp worker

// There are also sprintf/sprintfn functions for formatting data
// into a string, similar to String.Format.


type PersonWithAddress =
    {   Name: string
        Age: int
        Address: Address }
    static member Default =
        {   Name = "Phillip"
            Age = 12
            Address = Address.Default}

and Address = 
    {   Street : string 
        PostCode : int
        City : string}
    static member Default = {
        PostCode = 67100
        City = "Strasbourg" 
        Street = "Le CNAM"
    }

let Philip = PersonWithAddress.Default // Personne par défaut

let Morris = { PersonWithAddress.Default with Age = 81; Name= "Morris" }
let Jon = { PersonWithAddress.Default with Age = 12; Name= "Jon" ; Address = { City = "Tokyo"; PostCode = 1234; Street = "Akibahara"} }

let AfficherHabitation (personne :PersonWithAddress) = 
    match personne with 
    | p when (p.Address = Address.Default) -> printfn " %s n'a pas renseigné son adresse." personne.Name 
    | p when p.Address.Street = Address.Default.Street || p.Address.PostCode = Address.Default.PostCode || p.Address.PostCode = Address.Default.PostCode -> printfn "%s n'a pas renseigné toute son adresse" personne.Name
    | _ -> printfn "%s habite à : %A" personne.Name personne.Address

AfficherHabitation Morris
AfficherHabitation Jon


module M =
    let f x = nameof x

printfn $"{(M.f 12)}"
printfn $"{(nameof M)}"
printfn $"{(nameof M.f)}"

// On peut passer un argument facilement en l'écrivant juste après le nom de la fonction
let HelloCompose nom = printfn "Hello %s" nom
// Pour l'appeler, on écrit juste le nom de la fonction, puis l'argument qu'on veut lui passer. 
HelloCompose "Edouard"

let Addition i1 i2 = i1 + i2

// Si on veut spécifier le type de chaque argument, on peut le faire de la manière suivante : 
let add (x: int) (y: int): int = x + y

// Explicit type annotation (also for parameters)
Addition 1 2 |> printfn "Le résultat de l'addition de 1 et 2 est : %i" 
add 4 2 |> printfn "Le résultat de l'addition de 4 et 2 est : %i" 


// Définir une fonction composée également, on utilise l'indentation comme en python
let DemanderNom() = 
    printfn "Quel est votre nom ?"
    Console.ReadLine() // Ne pas oublier les parenthèses pour appeler la fonction

// On peut ensuite assigner le résultat à une variable
let prenom = DemanderNom()

// On peut ensuite utiliser la variable pour la passer à une fonction. 
HelloCompose prenom

// On peut aussi utiliser directement l'appel d'une fonction pour le passer à une autre fonction
// DemanderNom() |> HelloCompose

// On peut faire appel à la récursivité pour des fonctions avec le mot clé rec
let rec Factorielle x = 
    if x <= 1 then 1
    else 
        printfn "Factorielle %i = %i * Factorielle %i" x x (x-1)
        x * Factorielle (x - 1)

printfn "Factorielle 5 = %i" (Factorielle 5)

// Contient une liste des entiers de 1 à 20
let uneListe = [1..20]

let travaillerSurLesListes liste = 
    liste 
    |> List.filter (fun x -> x % 2 =0) // Filtre des nombres pairs
    |> List.map(fun x -> x * x)     // met les nombres au carré
    |> printfn ("Liste des nombres pairs élevés au carré : %A") // Affiche la liste résultat

travaillerSurLesListes uneListe

let afficherLesPuissancesDeDeuxAvecBoucle limite = 
    printf " Puissances de 2 de 1 à %i : " limite
    let mutable cpt = 1
    while cpt <= limite do 
        printf "%.0f " (2.0 ** (float cpt)) 
        cpt <- (cpt + 1)
    printfn ""

afficherLesPuissancesDeDeuxAvecBoucle 10 
// Fonction récursive, on n'utilise pas de if/else mais le pattern matching. 
let rec afficherLesPuissancesDeDeuxAvecRecursion x = 
    match x with
        | 0 -> printf " Puissances de 2 : "
        | _ ->  afficherLesPuissancesDeDeuxAvecRecursion(x - 1)
                printf "%.0f " (2.0 ** (float x)) // appeler float et lui passer un argument fait un cast de l'argument en float

afficherLesPuissancesDeDeuxAvecRecursion 10 
printfn "" 


// Une fonctionnalité très sympathique pour remplacer le if/elif/else ou les switch/case : le pattern matching
let fizzBuzz limit = 
    let fizzBuzzTranslator x = 
        match x with 
        | i when (i % 15 = 0) -> printfn "FizzBuzz"
        | i when (i % 5 = 0) -> printfn "Buzz"
        | i when (i % 3 = 0) -> printfn "Fizz"
        | _ -> printfn "%i" x
    for y in 1..limit do fizzBuzzTranslator y

// fizzBuzz 30 // Décommenter pour appeler fizzbuzz jusquà 30 

// Une énum est un type 
type emotion = 
    | joy = 0
    | anger = 1
    | fear = 2 

let print_feelings feel= 
    match feel with 
    | emotion.joy -> printfn "Im happy"
    | emotion.anger -> printfn "Im angry"
    | emotion.fear -> printfn "Im afraid"
    | _ -> printfn "Meh..."

// une enum peut contenir différents types
type Numbers = 
    | Entiers of int
    | Reels of float
    | Invalides // Pas de type associé
    
type EnumComposee = 
    | Emotion of emotion
    | Random // Pas de type associe 
    | RandomNumber of int

let ExempleEnumComposee (arg : EnumComposee) : bool = 
    match arg with 
    | Random -> false
    | RandomNumber r when r < 10 -> true 
    | RandomNumber r -> false 
    | Emotion e when e = emotion.joy -> true
    | Emotion e when (e = emotion.fear || e = emotion.anger) -> false
    | _ -> true

let months =
    [
        "January"; "February"; "March"; "April";
        "May"; "June"; "July"; "August"; "September";
        "October"; "November"; "December"
    ]


let lookupMonth month =
    if (month > 12 || month < 1) then
        invalidArg (nameof month) ($"Value passed in was %d{month}.")

    months[month-1]

printfn "%s" (lookupMonth 12)
printfn "%s" (lookupMonth 1)
printfn "%s" (lookupMonth 13)

let lastWeek :int [] = [| 1; 0; 5; 0; 12; 0; 2 |]
